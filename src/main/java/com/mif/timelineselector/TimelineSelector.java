package com.mif.timelineselector;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

/**
 * This class allows you to select the start and end time of something like video.
 * It resizes and proportionizes according to video time length
 */
public class TimelineSelector extends RelativeLayout implements ViewTreeObserver.OnGlobalLayoutListener, View.OnTouchListener {

    private static final String TAG = TimelineSelector.class.getSimpleName();

    private static final float PROPORTION_TO_TIME_MIN = 6f;

    private static final int BRACKET_WIDTH_DEFAULT = 14;

    private static final int POSITION_RIGHT = 1;
    private static final int POSITION_LEFT = 0;
    private static final int POSITION_NONRELATIVE = -1;

    private View mVideoRepresentation;

    private MHorizontalScroll mScrollContainer;

    private View mVLeftBracket;
    private View mVRightBracket;
    private View mVLeftBracketOuter;
    private View mVRightBracketOuter;
    private View mVInnerSelection;

    private long mVideoSelectionStartTime;
    private long mVideoSelectionEndTime;
    private long mVideoOverallTime;
    private long mVideoSelectionMinimumTime;

    private float proportionSecondsToPixel = PROPORTION_TO_TIME_MIN;

    private float mLastTouchOnLeftX;
    private float mLastTouchOnRightX;
    private int mLastTouchPosition;

    private boolean lockScroll = false;

    private SelectionListener mSelectionListener;
    private ScrollListener mScrollListener;

    private MHorizontalScroll.ScrollListener scrollListener = new MHorizontalScroll.ScrollListener() {
        @Override
        public void onScrollChanged(int newLeft, int newTop, int oldLeft, int oldTop) {
            callListener();
        }
    };

    public TimelineSelector(Context context) {
        super(context);

        initialize(null);
    }

    public TimelineSelector(Context context, AttributeSet attrs) {
        super(context, attrs);

        initialize(attrs);
    }

    public TimelineSelector(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initialize(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TimelineSelector(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        initialize(attrs);
    }

    /**
     * Creates all the view content for our view.
     * ScrollView at the bottom with the video-representation-view in it;
     * All brackets view stuff like brackets and boundaries between them and the parent.
     */
    private void initialize(AttributeSet attrs) {
        // Probable fix for some on 9-path drawables
        setPadding(0, 0, 0, 0);

        mScrollContainer = new MHorizontalScroll(getContext());
        final LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        mScrollContainer.setLayoutParams(params);
        mScrollContainer.setFillViewport(true);
        mScrollContainer.setOverScrollMode(View.OVER_SCROLL_NEVER);
        mScrollContainer.setSmoothScrollingEnabled(true);
        mScrollContainer.setOnScrollListener(scrollListener);
        FrameLayout innerContainer = new FrameLayout(getContext());
        innerContainer.setLayoutParams(params);

        mVideoRepresentation = new View(getContext());
        innerContainer.addView(mVideoRepresentation);

        mScrollContainer.addView(innerContainer);

        addView(mScrollContainer);

        // Creating brackets
        LayoutParams bracketParams = new LayoutParams(BRACKET_WIDTH_DEFAULT, ViewGroup.LayoutParams.MATCH_PARENT);

        mVRightBracket = new View(getContext());
        mVRightBracket.setLayoutParams(bracketParams);
        mVRightBracket.setId(R.id.right_bracket);
        // Probable fix for some on 9-path drawables
        mVRightBracket.setPadding(0, 0, 0, 0);
        // Bounded
        mVRightBracketOuter = new View(getContext());
        LayoutParams rightBoundsParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        mVRightBracketOuter.setLayoutParams(rightBoundsParams);

        mVLeftBracket = new View(getContext());
        mVLeftBracket.setLayoutParams(bracketParams);
        mVLeftBracket.setId(R.id.left_bracket);
        // Probable fix for some on 9-path drawables
        mVLeftBracket.setPadding(0, 0, 0, 0);
        // Bounded
        mVLeftBracketOuter = new View(getContext());
        LayoutParams leftBoundsParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        mVLeftBracketOuter.setLayoutParams(leftBoundsParams);

        // Between
        mVInnerSelection = new View(getContext());
        LayoutParams innerBoundsParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        mVInnerSelection.setLayoutParams(innerBoundsParams);

        addView(mVLeftBracketOuter);
        addView(mVLeftBracket);
        addView(mVRightBracket);
        addView(mVRightBracketOuter);
        addView(mVInnerSelection);

        setOnTouchListener(this);

        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.TimelineSelector);

            int bracketLeftResource = typedArray.getResourceId(R.styleable.TimelineSelector_bracketLeft_drawable, 0);
            if (bracketLeftResource > 0) {
                mVLeftBracket.setBackgroundResource(bracketLeftResource);
            }

            int bracketRightResource = typedArray.getResourceId(R.styleable.TimelineSelector_bracketRight_drawable, 0);
            if (bracketRightResource > 0) {
                mVRightBracket.setBackgroundResource(bracketRightResource);
            }

            int bracketWidth = typedArray.getDimensionPixelOffset(R.styleable.TimelineSelector_bracket_Width, BRACKET_WIDTH_DEFAULT);
            ViewGroup.LayoutParams leftParams = mVLeftBracket.getLayoutParams();
            leftParams.width = bracketWidth;
            mVLeftBracket.setLayoutParams(leftParams);
            ViewGroup.LayoutParams rightParams = mVRightBracket.getLayoutParams();
            rightParams.width = bracketWidth;
            mVRightBracket.setLayoutParams(rightParams);

            int bracketPadding = typedArray.getDimensionPixelSize(R.styleable.TimelineSelector_bracket_padding_vertical, 0);
            LayoutParams scrollParams = (LayoutParams) mScrollContainer.getLayoutParams();
            scrollParams.setMargins(scrollParams.leftMargin, bracketPadding, scrollParams.rightMargin, bracketPadding);
            mScrollContainer.setLayoutParams(scrollParams);
            // And for bounds too
            LayoutParams lbParams = (LayoutParams) mVLeftBracketOuter.getLayoutParams();
            lbParams.setMargins(lbParams.leftMargin, bracketPadding, lbParams.rightMargin, bracketPadding);
            mVLeftBracketOuter.setLayoutParams(lbParams);
            LayoutParams rbParams = (LayoutParams) mVRightBracketOuter.getLayoutParams();
            rbParams.setMargins(rbParams.leftMargin, bracketPadding, rbParams.rightMargin, bracketPadding);
            mVRightBracketOuter.setLayoutParams(rbParams);

            int outerResource = typedArray.getResourceId(R.styleable.TimelineSelector_outer, 0);
            if (outerResource > 0) {
                mVLeftBracketOuter.setBackgroundResource(outerResource);
                mVRightBracketOuter.setBackgroundResource(outerResource);
            }

            int innerResource = typedArray.getResourceId(R.styleable.TimelineSelector_inner, 0);
            if (innerResource > 0) {
                mVInnerSelection.setBackgroundResource(innerResource);
            }

            typedArray.recycle();
        }
    }

    public void setVideoRepresentation(@DrawableRes int resDrawable) {
        mVideoRepresentation.setBackgroundResource(resDrawable);
    }

    public void setBoundsResources(@DrawableRes int resOuterBounds, @DrawableRes int innerBounds) {
        mVRightBracketOuter.setBackgroundResource(resOuterBounds);
        mVLeftBracketOuter.setBackgroundResource(resOuterBounds);
        mVInnerSelection.setBackgroundResource(innerBounds);
    }

    public void setBracketResource(@DrawableRes int left, @DrawableRes int right) {
        mVRightBracket.setBackgroundResource(right);
        mVLeftBracket.setBackgroundResource(left);
    }

    public void setVideoLength(long videoLengthMillis) {
        this.mVideoOverallTime = videoLengthMillis;
        calculateViewToTimeProportion();
    }

    public void setSelectedPiece(long videoSelectionStart, long videoSelectionEnd) {
        this.mVideoSelectionStartTime = videoSelectionStart;
        this.mVideoSelectionEndTime = videoSelectionEnd;
    }

    public void setMinimumSelectedLengthInMilliseconds(long minSelection) {
        this.mVideoSelectionMinimumTime = minSelection;
    }

    public void setMinimumSelectedLenghtInPixels(int pixels) {
        this.mVideoSelectionMinimumTime = calculatePixelsToTime(pixels);
    }

    private void calculateViewToTimeProportion() {
        if (getWidth() < 1) {
            getViewTreeObserver().addOnGlobalLayoutListener(this);
        }
    }

    private void callListener() {
        updateTime();

        if (mSelectionListener != null) {
            mSelectionListener.onSelectionChanged(mVideoSelectionStartTime, mVideoSelectionEndTime);
        }
    }

    public void setSelectionListener(SelectionListener listener) {
        this.mSelectionListener = listener;
    }

    private void updateTime() {
        mVideoSelectionEndTime = calculatePixelsToTime((int) (mVRightBracket.getX() + mVRightBracket.getWidth() + mScrollContainer.getScrollX()));
        mVideoSelectionStartTime = calculatePixelsToTime((int) (mVLeftBracket.getX() + mScrollContainer.getScrollX()));
    }

    @Override
    public void onGlobalLayout() {
        getViewTreeObserver().removeOnGlobalLayoutListener(this);

        long videoSeconds = mVideoOverallTime / 1000;
        int videoRepresentationWidth = getWidth();
        if (getWidth() <= videoSeconds) {
            videoRepresentationWidth = (int) (videoSeconds * PROPORTION_TO_TIME_MIN);
        }
        mVideoRepresentation.getLayoutParams().width = videoRepresentationWidth;
        proportionSecondsToPixel = videoSeconds / (float) videoRepresentationWidth;

        int scrollOffsetMax = videoRepresentationWidth - getWidth();
        int scrollOffset = 0;

        // Calculate position of the end bracket
        int rightBracketX = (int) (mVideoSelectionEndTime / 1000 / proportionSecondsToPixel);

        if (rightBracketX > (getWidth() - mVRightBracket.getWidth())) {
            // Bracket would be outside - bad thing
            scrollOffset = rightBracketX - getWidth() - mVRightBracket.getWidth();
        }
        rightBracketX -= scrollOffset;

        // Calculate position of the start bracket
        int leftBracketX = (int) (mVideoSelectionStartTime / 1000 / proportionSecondsToPixel);
        leftBracketX -= scrollOffset;

        updateStartBracket(leftBracketX, true);
        updateEndBracket(rightBracketX);

        mScrollContainer.setScrollX(scrollOffset);

        // Make sure everyone now we're ready!
        callListener();
    }

    private int getTouchPosition(MotionEvent event) {
        if (event.getX() <= (mVLeftBracket.getX() + mVLeftBracket.getWidth())) {
            return POSITION_LEFT;
        } else if (event.getX() >= mVRightBracket.getX()) {
            return POSITION_RIGHT;
        } else {
            return POSITION_NONRELATIVE;
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return getTouchPosition(ev) != POSITION_NONRELATIVE;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                lockScroll = true;
                mLastTouchPosition = getTouchPosition(motionEvent);
                break;

            case MotionEvent.ACTION_UP:
                lockScroll = false;
                mLastTouchPosition = POSITION_NONRELATIVE;
                break;

        }

        if (mScrollListener != null) {
            mScrollListener.requireScrollLock(lockScroll);
        }

        if (mLastTouchPosition != POSITION_NONRELATIVE) {
            maintainTouchEvent(motionEvent, mLastTouchPosition);
            return true;

        } else {
            return false;
        }
    }

    private void maintainTouchEvent(MotionEvent event, int position) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_MOVE:
                float offset = 0f;

                if (position == POSITION_LEFT) {
                    offset = mLastTouchOnLeftX - event.getX();

                } else if (position == POSITION_RIGHT) {
                    offset = mLastTouchOnRightX - event.getX();
                }


                if (Math.abs(offset) < proportionSecondsToPixel) {
                    return;
                }

                moveBracketAndUpdateTime(offset, position);
                break;
        }

        if (position == POSITION_LEFT) {
            mLastTouchOnLeftX = event.getX();

        } else if (position == POSITION_RIGHT) {
            mLastTouchOnRightX = event.getX();
        }
    }

    /**
     * Moves brackets and calls time selection listener if specified
     *
     * @param offset
     * @param position
     */
    private void moveBracketAndUpdateTime(float offset, int position) {
        boolean updated = false;
        if (position == POSITION_LEFT) {
            updated = updateStartBracket((int) (mVLeftBracket.getX() - offset + mVLeftBracket.getWidth() / 2), false);

        } else if (position == POSITION_RIGHT) {
            updated = updateEndBracket((int) (mVRightBracket.getX() - offset + mVRightBracket.getWidth() / 2));
        }

        if (updated) {
            callListener();
        }
    }

    /**
     * Updates the position and time for the left bracket a.k.a {@code mVideoSelectionStartTime}
     *
     * @param leftBracketX
     * @return true if bracket is updated
     */
    private boolean updateStartBracket(int leftBracketX, boolean useForce) {
        leftBracketX -= mVLeftBracket.getWidth() / 2;
        if (!useForce) {
            leftBracketX = correctTrespassing(POSITION_LEFT, leftBracketX);
        }

        if (Math.abs(leftBracketX - mVLeftBracket.getX()) > 0.01f || useForce) {
            mVLeftBracket.setX(Math.max(leftBracketX, 0));

            // Update boundaries views
            mVLeftBracketOuter.setX(0);
            ViewGroup.LayoutParams layoutParams = mVLeftBracketOuter.getLayoutParams();
            layoutParams.width = leftBracketX;
            mVLeftBracketOuter.setLayoutParams(layoutParams);

            return true;

        } else {
            return false;
        }
    }

    /**
     * Updates the position and time for the right bracket a.k.a {@code mVideoSelectionEndTime}
     *
     * @param rightBracketX
     * @return true if bracket is updated
     */
    private boolean updateEndBracket(int rightBracketX) {
        int actualX = rightBracketX - mVLeftBracket.getWidth() / 2;
        actualX = correctTrespassing(POSITION_RIGHT, actualX);

        if (Math.abs(actualX - mVRightBracket.getX()) > 0.01f) {
            mVRightBracket.setX(actualX);

            // Update boundaries views
            mVRightBracketOuter.setX(actualX + mVRightBracket.getWidth());
            ViewGroup.LayoutParams layoutParams = mVRightBracketOuter.getLayoutParams();
            layoutParams.width = getWidth() - rightBracketX;
            mVRightBracketOuter.setLayoutParams(layoutParams);

            return true;

        } else {
            return false;
        }
    }

    /**
     * Corrects provided X position so it won't go out ouf bounds or collide with other bracket or mess with minimum time
     *
     * @param position {@code POSITION_LEFT} or {@code POSITION_RIGHT}
     * @param newX     final calculated X position of the bracket
     * @return corrected X
     */
    private int correctTrespassing(int position, int newX) {
        if (position == POSITION_LEFT) {
            if (newX + mVLeftBracket.getWidth() > mVRightBracket.getX()) {
                // Correct position relatively to another bracket
                newX = (int) (mVRightBracket.getX() - mVLeftBracket.getWidth());
            }

            if (newX < 0) {
                // Correct position relatively to end
                newX = 0;
            }

        } else if (position == POSITION_RIGHT) {
            if (newX < (mVLeftBracket.getX() + mVLeftBracket.getWidth())) {
                // Correct position relatively to another bracket
                newX = (int) (mVLeftBracket.getX() + mVLeftBracket.getWidth());
            }

            if ((newX + mVRightBracket.getWidth()) > getWidth()) {
                // Correct position relatively to end
                newX = getWidth() - mVRightBracket.getWidth();
            }
        }

        boolean allowByTiming = calculatePixelsToTime(calculateSelectionWidthInPixels(position, newX)) >= mVideoSelectionMinimumTime;
        if (!allowByTiming) {
            // TODO: someTIMEs it could produce flickers, so you better somehow figure it out! So for now it'll only revert X position
            if (position == POSITION_LEFT) {
                newX = (int) mVLeftBracket.getX();
//                newX = (int) ((mVideoSelectionEndTime - mVideoSelectionMinimumTime) / 1000 / proportionSecondsToPixel)
//                        + mVLeftBracket.getWidth() / 2;

            } else if (position == POSITION_RIGHT) {
                newX = (int) mVRightBracket.getX();
//                newX = (int) ((mVideoSelectionStartTime + mVideoSelectionMinimumTime) / 1000 / proportionSecondsToPixel)
//                - mVRightBracket.getWidth() / 2;
            }
        }

        return newX;
    }

    /**
     * Calculates width in pixels between brackets (or just call it selection)
     *
     * @param position {@code POSITION_RIGHT} or {@code POSITION_LEFT} - position of the "moved" bracket
     * @param newX     measured X of the moved bracket
     * @return Width in pixels which will be if {@code newX} would be applied
     */
    private int calculateSelectionWidthInPixels(int position, int newX) {
        if (position == POSITION_LEFT) {
            int selectionStartX = newX;
            int selectionEndX = (int) mVRightBracket.getX() + mVRightBracket.getWidth();

            return selectionEndX - selectionStartX;

        } else if (position == POSITION_RIGHT) {
            int selectionStartX = (int) (mVLeftBracket.getX());
            int selectionEndX = newX + mVRightBracket.getWidth();

            return selectionEndX - selectionStartX;

        } else {
            return 0;
        }
    }

    /**
     * Converts length in pixels to time in milliseconds according to proportions
     *
     * @param pixels the number of pixels in time segment
     * @return proportional time in milliseconds
     */
    private long calculatePixelsToTime(int pixels) {
        return (long) (pixels * proportionSecondsToPixel * 1000);
    }

    /**
     * Returns array with 2 brackets. You can use this in case you want to somehow bind anything to bracket views f.e. their time.
     * Please try not to modify this views.
     *
     * @return
     */
    public float[] getBracketsCoordinates() {
        return new float[]{mVLeftBracket.getX(), mVRightBracket.getX() + mVRightBracket.getWidth()};
    }

    /**
     * Use this check whenever you decide to let upper Scroll/List-View to their thing
     *
     * @return true if we can give away our scroll, false if we're currently interacting with timeline
     */
    public boolean isScrollingAllowed() {
        return !lockScroll;
    }

    /**
     * Sets a listener to look for timeline scroll events
     * @param listener
     */
    public void setScrollListener(ScrollListener listener) {
        this.mScrollListener = listener;
    }

    public interface ScrollListener {
        void requireScrollLock(boolean lockScroll);
    }

    public interface SelectionListener {
        void onSelectionChanged(long startSelection, long endSelection);
    }
}
