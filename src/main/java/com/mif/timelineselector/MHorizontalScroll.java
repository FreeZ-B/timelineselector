package com.mif.timelineselector;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.HorizontalScrollView;

/**
 * Created by Pavel Barmin on 12.07.16
 */
class MHorizontalScroll extends HorizontalScrollView {
    private ScrollListener mScrollListener;

    public MHorizontalScroll(Context context) {
        super(context);
    }

    public MHorizontalScroll(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MHorizontalScroll(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MHorizontalScroll(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);

        if (mScrollListener != null) {
            mScrollListener.onScrollChanged(l, t, oldl, oldt);
        }
    }

    public void setOnScrollListener(ScrollListener l) {
        this.mScrollListener = l;
    }

    public interface ScrollListener {
        void onScrollChanged(int newLeft, int newTop, int oldLeft, int oldTop);
    }
}
